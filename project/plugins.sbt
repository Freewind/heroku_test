// Comment to get more information during initialization
logLevel := Level.Warn

// The Typesafe repository
resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

// Use the Play sbt plugin for Play projects

// 在heroku上，要改为
// addSbtPlugin("play" % "sbt-plugin" % "2.0-RC3")

// 如果在本地，则为
addSbtPlugin("play" % "sbt-plugin" % "2.0-SNAPSHOT")
