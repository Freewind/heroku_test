package controllers

import play.api._
import play.api.mvc._

object Application extends Controller {

  def index = Action {
    val chineseMessage = "中文"
    Ok(views.html.index("Your new application is ready.", chineseMessage))
  }

  def test1 = Action {
    val f = Play.current.getFile("app/views/main.scala.html")
    val content = scala.io.Source.fromFile(f).mkString
    Ok(content)
  }
  
  def test2 = Action {
    val f = Play.current.getFile("target/scala-2.9.1/src_managed/main/views/html/main.template.scala")
    val content = scala.io.Source.fromFile(f).mkString
    Ok(content)
  }

}